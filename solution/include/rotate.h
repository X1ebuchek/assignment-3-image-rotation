#ifndef ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
#include "image.h"

struct image rotate(const struct image* old_image);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_ROTATE_H
