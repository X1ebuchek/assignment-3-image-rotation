#ifndef ASSIGNMENT_3_IMAGE_ROTATION_PIXEL_H
#define ASSIGNMENT_3_IMAGE_ROTATION_PIXEL_H
#include "image.h"
#include <stdint.h>

void set_pixel(const struct image* image, const uint64_t x, const uint64_t y, const struct pixel new_pixel);
struct pixel get_pixel(const struct image* image, const uint64_t x, const uint64_t y);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_PIXEL_H
