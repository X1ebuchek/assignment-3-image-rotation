//
// Created by Huawei on 05.12.2022.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_FILE_H
#include <stdbool.h>
#include <stdio.h>

bool open_file_wb(FILE** file, const char* file_name );
bool open_file_rb(FILE** file, const char* file_name);
bool close_file(FILE *file);

#endif //ASSIGNMENT_3_IMAGE_ROTATION_FILE_H
