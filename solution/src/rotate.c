#include "pixel.h"
#include "rotate.h"

#include "stdio.h"
struct image rotate(const struct image* old_image){

    struct image new_image = create_image(old_image->height, old_image->width);

    for (size_t height_count = 0; height_count < old_image->height; height_count++){
        for (size_t width_count = 0; width_count < old_image->width; width_count++){
            set_pixel(&new_image, (old_image->height - 1) - height_count, width_count,
                      get_pixel(old_image, width_count, height_count)
            );
        }
    }
    return new_image;
}

