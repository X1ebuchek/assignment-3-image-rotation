#include "bmp.h"
#include "file.h"
#include "image.h"
#include "rotate.h"
#include <stdio.h>

int main( int argc, char** argv ) {
    if (argc != 3){
        printf("Not 2 arguments ");
        return 1;
    }

//    FILE* in = fopen(argv[1], "rb");
    FILE* in = NULL;
    if (!open_file_rb(&in,argv[1])){
        printf("Input image is error ");
        return 1;
    }

    struct image img = {0};

    enum read_status status = from_bmp(in, &img);
    if (status != READ_OK){
        if (status == READ_INVALID_BITS){
            printf("Bits error ");
        } else if (status == READ_INVALID_HEADER){
            printf("Header error ");
        } else if(status == READ_INVALID_SIGNATURE){
            printf("Signature error ");
        }
        free_image(&img);
        return 1;
    }

    if (!close_file(in)){
        printf("Error closing file ");
        free_image(&img);
        return 1;
    };

    struct image output_image = rotate(&img);
    free_image(&img);

//    FILE* out = fopen(argv[2], "wb");
    FILE* out = NULL;
    if (!open_file_wb(&out,argv[2])){
        printf("Error opening file ");
        free_image(&output_image);
        return 1;
    }

    enum write_status write_status = to_bmp(out, &output_image);
    if (write_status != WRITE_OK){
        printf("Error writing file ");
        free_image(&output_image);
        return 1;
    }

    if (!close_file(out)){
        printf("Error closing file ");
        free_image(&output_image);
        return 1;
    };

    free_image(&output_image);
    printf("Done!\n Your image is https://clck.ru/3vyXS ");
    return 0;
}

