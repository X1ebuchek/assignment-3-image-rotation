#include "image.h"
#include "malloc.h"
#include "pixel.h"

#include "stdint.h"

struct image create_image(const uint64_t width, const uint64_t height){
    struct image image = {0};

    image.width = width;
    image.height = height;

    struct pixel* data = malloc(width * height * sizeof(struct pixel));

    image.data = data;
    return image;
}

void free_image(struct image* image){
    free(image->data);
}

