#include "pixel.h"

void set_pixel(const struct image* image, const uint64_t x, const uint64_t y, const struct pixel new_pixel){
    uint64_t arg = y * image->width + x;
    image->data[arg] = new_pixel;
}

struct pixel get_pixel(const struct image* image, const uint64_t x, const uint64_t y){
    uint64_t arg = y * image->width + x;
    return image->data[arg];
}
