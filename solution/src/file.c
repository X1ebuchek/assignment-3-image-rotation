#include "file.h"
bool open_file_wb(FILE** file, const char* file_name ){
    *file = fopen(file_name, "wb");

    if (*file == NULL){
        return false;
    }
    else return true;
}
bool open_file_rb(FILE** file, const char* file_name){
    *file = fopen(file_name, "rb");

    if (*file == NULL){
        return false;
    }
    else return true;
}

bool close_file(FILE* file) {
    return fclose(file) == 0;
}

