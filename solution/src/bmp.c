#include "bmp.h"
#include "malloc.h"
#define BF_TYPE 19778
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BIT_COUNT 24
#define BI_COMPRESSION 0
#define ZERO 0

size_t get_padding(size_t width){
    return (4 - (sizeof(struct pixel) * width)) % 4;
}

struct bmp_header image_get_header(const struct image* image){

    size_t size = (image->width + get_padding(image->width)) * image->height * sizeof(struct pixel);

    return (struct bmp_header){
            .bfType = BF_TYPE,
            .bfileSize = size + sizeof(struct bmp_header),
            .bfReserved = BF_RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BI_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BIT_COUNT,
            .biCompression = BI_COMPRESSION,
            .biSizeImage = size,
            .biXPelsPerMeter = ZERO,
            .biYPelsPerMeter = ZERO,
            .biClrUsed = ZERO,
            .biClrImportant = ZERO,
    };
}

enum read_status from_bmp(FILE* const in, struct image* image){
    struct bmp_header header = {0};

    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1){
        return READ_INVALID_HEADER;
    }

    *image = create_image(header.biWidth, header.biHeight);

    size_t padding = get_padding(header.biWidth);

    for (size_t i = 0; i < image->height; ++i){
        if (fread(image->data + (i * image->width), sizeof(struct pixel), image->width, in) != image->width) {
            free_image(image);
            return READ_INVALID_SIGNATURE;
        }

        if (fseek(in, (long) padding, SEEK_CUR) != 0) {
            free_image(image);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* const out, struct image const* image){
    struct bmp_header header = image_get_header(image);

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)){
        return WRITE_ERROR;
    }

    size_t padding = get_padding(image->width);
    uint8_t zero_padding = 0;

    for (size_t i = 0; i < image->height; i++){
        if (fwrite(image->data + (i * image->width), sizeof(struct pixel), image->width, out) != image->width){
            return WRITE_ERROR;
        }

        for (size_t j = 0; j < padding; j++){
            if (!fwrite(&zero_padding, sizeof(uint8_t), 1, out)){
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}

